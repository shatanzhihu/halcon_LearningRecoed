# Halcon_LearningRecoed

#### 介绍
记载一些在编程及图像处理过程中的问题及解决方案

#### 软件环境
Qt：5.9.6
vs2019
Halcon 12.0/17.0


#### 子项目介绍

1.  ARM_FontSet

![ARM_FontSet](https://images.gitee.com/uploads/images/2021/0817/221328_6315a907_4968621.png "1.ARM_FontSet.PNG")

2.  CustomView

![CustomView](https://images.gitee.com/uploads/images/2021/0817/221415_1799756c_4968621.png "2.CustomView.PNG")

3.  ImageScale

![ImageScale](https://images.gitee.com/uploads/images/2021/0817/221456_5b998351_4968621.png "3.ImageScale.PNG")

4.Draw_Item

![Draw_Item](https://images.gitee.com/uploads/images/2021/0817/221520_bf81174f_4968621.png "4.Draw_Item.PNG")

#### 文件夹说明

1.  snap
此文件夹为各子工程界面截图
2.  File_Info
此文件夹存放工程所需文件，如图像/txt/ini
3.  bin
此文件夹存放工程创建的exe文件


